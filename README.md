# MOVED

The code has been moved to: https://github.com/boundfoxstudios/basic-idle-engine

# Basic Idle Engine

This repository contains a basic idle engine to make your own games.
It was built for a YouTube video, that can be watched here: https://www.youtube.com/watch?v=ZiugwSysFVY.

The basic idle engine uses ScriptableObjects to define the generators and the session.
Feel free to adapt the code for your needs.
